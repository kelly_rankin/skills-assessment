'use strict';

var contacts_model = null;

var currentContact = undefined;

var contact_path = 'http://localhost:8080/widget/js/contacts.json';

var view_options = [ 
									{ 'option':'Email',
									   'value': 'email'
									},
									{ 'option':"Phone",
										'value': 'phone'
									}
								 ];
								 

function getContacts() {
		
	var deferred = $.Deferred();
	
	setTimeout( 
		function() {
		
			$.ajax(
				{
					type: "GET",
					dataType: "json",
					url: contact_path,
					success: function(data) {
						deferred.resolve(data);
					},
					error: function(data) {
						console.log(data.responseText);
					},
				}) 
				}
			, 2000);
	
	return deferred.promise();			
}

function buildWidget(data) {
	
	contacts_model = data;
	
	addHeader();
	addContacts();
	addFooter();

	toggleView();
	
	$('#display-select').onchange = function () {
		toggleView();
	};
	
	var overlay = $('<div>');
	
	overlay.addClass('overlay');
	
	$('#contacts').append(overlay);
	
	toggleOverlay(false);
	
	$('.contact').each(function(){
	
		$(this).mouseenter(function() {
		
			var focusedContact = $(this);
			
			if(currentContact != undefined) {
				unselectContact(currentContact);
			}
			
			toggleOverlay(true);
						
			focusedContact.addClass('selected-contact');
			
			var index = getContactIndex(focusedContact);
			displayContactInfo(index, getContactInfoElem(this));
			currentContact = focusedContact;

		});
		
		$(this).mouseleave(function() {
				unselectContact(currentContact);
				toggleOverlay(false);
		});
		
	});
}

function unselectContact(contact) {
	contact.removeClass('selected-contact');
	var itemToRemove = getContactInfoDataElem(contact);
	$(itemToRemove[0]).remove();
	
	currentContact = undefined;
}

function toggleOverlay(showOverlay) {
	if(!showOverlay) {
		$('.overlay').hide();
	}
	else {
		$('.overlay').show();
	}
}	

function getContactIndex(elem) {
	return $(elem).find('input.contact-val').val();
}

function getContactInfoElem(elem) {
	return $(elem).find('.contact-info').eq(0);
}

function getContactInfoDataElem(elem) {
	return $(elem).parent().find('.contact-info-data').eq(0);
}

function addContacts() {

	//used for css to choose shade of cell
	var darkOrLight = 'dark';
					
	for(var contactIndex in contacts_model.contacts) {
	
		var aContact = contacts_model.contacts[contactIndex];
		addContact(aContact, contactIndex, darkOrLight);	
		
		darkOrLight = (darkOrLight  == "dark") ? "light" : "dark";
	}
}

function addContact(aContact, contactIndex, darkness) {
		var tempContact = $('<div>');
		
		tempContact.addClass('contact');
		tempContact.addClass('contact-' + darkness);
		
		var contactVal = $('<input>');
		contactVal.addClass('contact-val');
		contactVal.attr('type', 'hidden');
		contactVal.val(contactIndex);
		
		tempContact.append(contactVal);
		
		var onlineStatusHolder = $('<div>');
		onlineStatusHolder.addClass('online-status-holder');
		
		var onlineStatus = $('<div>');
		onlineStatus.addClass('online-status');
		onlineStatus.addClass('contact-' + translateStatus(aContact.status));
		
		onlineStatusHolder.append(onlineStatus);
		tempContact.append(onlineStatusHolder);
		
		var contactName = $('<div>');
		contactName.addClass('contact-name');
		contactName.text(aContact.firstName);
		
		tempContact.append(contactName);
		
		var contactInfo = $('<div>');
		contactInfo.addClass('contact-info');
		
		var contactEmail = $('<div>');
		contactEmail.addClass('contact-email');
		contactEmail.text(aContact.email);
		
		var contactPhone = $('<div>');
		contactPhone.addClass('contact-phone');
		contactPhone.text(aContact.phone);
		
		contactInfo.append(contactEmail);
		contactInfo.append(contactPhone);
		
		tempContact.append(contactInfo);
		
		$('#contacts').append(tempContact);
}

function addHeader() {
	$('#main-content').append('<link rel="stylesheet" type="text/css" href="css/contact.css">');
			
	var contacts = $('<div>');
	contacts.attr('id', 'contacts');
	
	var contactsHeader = $('<div>');
	contactsHeader.attr('id', 'contacts-header');
	
	var contactsSpan = $('<span>');
	contactsSpan.text('Contacts');
	
	contactsHeader.append(contactsSpan);
	
	contacts.append(contactsHeader);
	
	$('#contacts-container').append(contacts);
}

function addFooter() {

	var contactsFooter = $('<div>');
	contactsFooter.attr('id', 'contacts-footer');
	
	var optionsHolder = $('<div>');
	optionsHolder.attr('id', 'options-holder');
	
	var displaySelect = $('<select>');
	displaySelect.attr('id', 'display-select');
	displaySelect.change(function() {
		toggleView();
		
		if(currentContact != undefined) {
			unselectContact(currentContact);
		}
	});
	
	optionsHolder.append(displaySelect);
	
	contactsFooter.append(optionsHolder);

	$('#contacts').append(contactsFooter);
	
	for(var opt in view_options) {
	
		var displayOption = $('<option>');
		displayOption.val(opt.value);
		displayOption.text(view_options[opt].option);
		
		$('#display-select').append(displayOption);
	}
	
}

function translateStatus(value) {
	switch (value) {
		case '0':
			return 'present';
		case '1':
			return 'away';
		case '2':
			return 'busy';
	}
}

function toggleView() {

	var val = $('#display-select').find(":selected").text();
	
	if(val == 'Phone') {
		$('.contact-phone').css('display', '');
		$('.contact-email').css('display', 'none');
	}
	else {
		$('.contact-email').css('display','');
		$('.contact-phone').css('display', 'none');
	}
}

function displayContactInfo(contactIndex, displayContainer) {
	var contact = contacts_model.contacts[contactIndex];
	
	var contactInfoData = $('<div>');
	contactInfoData.addClass('contact-info-data');
	
	var mail = $('<div>');
	
	var mailTo = $('<a>');
	
	mailTo.attr('href', 'mailto:' + contact.email);
	mailTo.text(contact.email);
	
	mail.append(mailTo);
	
	contactInfoData.append(mail);
	
	var phone = $('<div>');
	phone.text(contact.phone);
	
	contactInfoData.append(phone);
	
	var address = $('<div>');
	address.addClass('contact-address');
	
	var streetAddressSpan = $('<span>');
	streetAddressSpan.addClass('street-address');
	
	streetAddressSpan.text(contact.streetAddress);
	
	address.append(streetAddressSpan);
	
	var cityStateZipSpan = $('<span>');
	
	cityStateZipSpan.text(contact.cityStateZip);
	
	address.append(cityStateZipSpan);
	
	contactInfoData.append(address);
	
	displayContainer.append(contactInfoData);
}
	
(function () {
	//we kinda want to get the latest file each time
	$.ajaxSetup({
		cache:false
	})
	
		getContacts().then(
		function(data) {
			buildWidget(data)
		}
	);
	
})();


